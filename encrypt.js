const NodeRSA = require('node-rsa');
const safeJsonStringify = require('safe-json-stringify');

const fail = function(callback, body, prefix) {
    callback(null, {"statusCode": 500, "body": (prefix ? prefix : '') + (body instanceof String ? body : safeJsonStringify(body))});
}

module.exports.encryptWithPublicKey = function(callback, publicKeyStr, unencryptedStr, successCallback) {
    const publicKey = new NodeRSA(publicKeyStr);

    try {
        successCallback(publicKey.encrypt(Buffer.from(unencryptedStr), 'base64'));
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to encrypt message with public key...");
    }
}

module.exports.decryptWithPublicKey = function(callback, publicKeyStr, encryptedStr, successCallback) {
    const publicKey = new NodeRSA(publicKeyStr);

    let decryptedBodyStr;
    try {
        decryptedBodyStr = publicKey.decryptPublic(encryptedStr, 'utf8');
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to decrypt message with public key...");
        return;
    }

    try {
        JSON.parse(decryptedBodyStr);
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to parse JSON from decrypted text: " + decryptedBodyStr);
        return;
    }

    successCallback(JSON.parse(decryptedBodyStr));
}

module.exports.encryptWithPrivateKey = function(callback, privateKeyStr, unencryptedStr, successCallback) {
    const privateKey = new NodeRSA(privateKeyStr);

    try {
        successCallback(privateKey.encryptPrivate(Buffer.from(unencryptedStr), 'base64'));
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to encrypt message with private key...");
    }
}

module.exports.decryptWithPrivateKey = function(callback, privateKeyStr, encryptedStr, successCallback) {
    const privateKey = new NodeRSA(privateKeyStr);

    let decryptedBodyStr;
    try {
        decryptedBodyStr = privateKey.decrypt(encryptedStr, 'utf8');
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to decrypt message with private key...");
        return;
    }

    try {
        JSON.parse(decryptedBodyStr);
    } catch (e) {
        console.log(e);
        fail(callback, "Failed to parse JSON from decrypted text: " + decryptedBodyStr);
        return;
    }

    successCallback(JSON.parse(decryptedBodyStr));
}