#!/usr/bin/env bash

touch buildArtifact.zip && rm buildArtifact.zip && zip -r buildArtifact.zip ./

aws s3  cp ./buildArtifact.zip  s3://pennidinh-code-assets/pennidinhadminserver-proxysockets.zip

aws lambda update-function-code --function-name PenniDinhCentral-ProxySockets --s3-bucket pennidinh-code-assets --s3-key pennidinhadminserver-proxysockets.zip --region us-west-2
