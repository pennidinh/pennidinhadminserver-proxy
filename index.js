const encrypt = require('./encrypt');
const safeJsonStringify = require('safe-json-stringify');
const crypto = require('crypto')
const AWS = require('aws-sdk');
// Set the Region
AWS.config.update({region: 'us-west-2'});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

const succeed = function(callback, body) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 200, 'body': (body instanceof String ? body : safeJsonStringify(body))});
}

const fail = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 400, 'body': (prefix ? prefix : '') + (body instanceof String ? body : safeJsonStringify(body))});
}

const fault = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 500, 'body': (prefix ? prefix : '') + (body instanceof String ? body : safeJsonStringify(body))});
}

CLIENT_ID_TABLE_NAME = process.env['ClientPublicKeysTableName'];
CLIENT_ID_HASH_KEY = "ClientId";
getPublicKeyForClientId = function(callback, clientId, successCallback) {
    const key = {};
    key[CLIENT_ID_HASH_KEY] = {
        S: clientId
    };
    const params = {
        Key: key,
        TableName: CLIENT_ID_TABLE_NAME
    };
    ddb.getItem(params, function(err, data) {
        if (err) {
            fault(callback, err, 'get item error: ');
            return;
        }

        if (data && data.Item) {
            successCallback(data.Item.PublicKey.S)
        } else {
            successCallback(null);
        }
    });
};

const uploadNewPublicKey = function(callback, clientId, sshPubKey, successCallback) {
    ddb.putItem({TableName: process.env.ClientSshPublicKeysTableName, Item: {'clientId': {S: clientId}, 'sshPubKey': {S: sshPubKey}}}, function(err, data) {
        if (err) {
            console.log("Error", err);
            fault(callback, err);
            return;
        }

        successCallback();
    });
}

exports.handler = function(event, context, callback) {
    console.log('Event: ' + safeJsonStringify(event));
    console.log('Context: ' + safeJsonStringify(context));
    console.log('Handling lambda invocation...');

    const path = event.requestContext.http.path.indexOf('/Prod') == 0 ? event.requestContext.http.path.substring('/Prod'.length) : event.requestContext.http.path;
    console.log('path: ' + path);

    if (path === '/getProxyEndpoint') {
        const externalHostName = event.queryStringParameters['externalHostName'] ? decodeURIComponent(event.queryStringParameters['externalHostName']) : '';
        console.log('externalHostName: ' + externalHostName);
        const protocol = event.queryStringParameters['protocol'];
        console.log('protocol: ' + protocol);

        if (!externalHostName) {
            fail(callback, 'externalHostName param must be defined!');
            return;
        }
        if (!protocol) {
            fail(callback, 'protocol param must be defined!');
            return;
        }

        const params = {
            TableName: process.env.SharedProxyAssignmentsDynamoDBTableName,
            Key: {
                'externalSubdomain' : {S: externalHostName},
                'service' : {S: protocol}
            }
        };

        ddb.getItem(params, function (err, data) {
            if (err) {
                console.log(err);
                fault(callback, err);
                return;
            }

            if (data.Item) {
                succeed(callback, {hostname: data.Item.hostname.S, port: parseInt(data.Item.port.N)});
            } else {
                callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 404, 'body': "Couldn't find item with externalHostName: " + externalHostName});
            }
        })
    } else if (path === '/fetchPubKey') {
        const hostname = event.queryStringParameters['hostname'] ? decodeURIComponent(event.queryStringParameters['hostname']) : '';
        console.log('hostname: ' + hostname);

        if (!hostname) {
            fail(callback, 'hostname');
            return;
        }

        const params = {
            TableName: process.env.SubdomainAssignmentsDynamoDBTableName,
            Key: {
                'Subdomain' : {S: hostname}
            }
        };
        ddb.getItem(params, function (err, data) {
            if (err) {
                console.log(err);
                fault(callback, err);
                return;
            }

            if (data.Item && data.Item.IsProxy && data.Item.IsProxy.BOOL) {
                if (!data.Item.ClientId || !data.Item.ClientId.S) {
                    console.log('Expected host: ' + hostame + ' to have a ClientId in its DDB entry!');
                    fault(callback, 'Expected host: ' + hostame + ' to have a ClientId in its DDB entry!');
                    return;
                }

                const params2 = {
                    TableName: process.env.ClientSshPublicKeysTableName,
                    Key: {
                        'clientId' : {S: data.Item.ClientId.S}
                    }
                };
 
                ddb.getItem(params2, function (err, data2) {
                    if (err) {
                        console.log(err);
                        fault(callback, err);
                        return;
                    }

                    if (!data2.Item) {
                        console.log('ClientId: ' + data.Item.ClientId.S + ' has not uploaded an ssh pub key yet');
                        fail(callback, 'ClientId: ' + data.Item.ClientId.S + ' has not uploaded an ssh pub key yet');
                        return;
                    }

                    if (!data2.Item.sshPubKey || !data2.Item.sshPubKey.S) {
                        console.log('Expected ClientId ' + data.Item.ClientId.S + ' to have an sshPubKey in its DDB entry!');
                        fault(callback, 'Expected ClientId ' + data.Item.ClientId.S + ' to have an sshPubKey in its DDB entry!');
                        return;
                    }

                    succeed(callback, {sshPubKey: data2.Item.sshPubKey.S});
                });
            } else {
                callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 404, 'body': "Couldn't find shared proxy host name: " + hostname});
            }
        })
    } else if (path === '/uploadPubKey') {
        const clientId = event.headers['x-pennidinh-clientid'];
        const token = event.headers['x-pennidinh-token'];

        console.log('ClientId: ' + clientId);
        console.log('Token: ' + token);

        getPublicKeyForClientId(callback, clientId, function(clientPubKey) {
            encrypt.decryptWithPublicKey(callback, clientPubKey, token, function(decryptedToken) {
                console.log('DecryptedToken: ' + safeJsonStringify(decryptedToken));

                const createdAt = decryptedToken.createdAt;
                console.log('Created at: ' + createdAt);

                const pubKeyMD5 = decryptedToken.pubKeyMD5;
                console.log('Pub Key MD5: ' + pubKeyMD5);

                const pubKey = event.isBase64Encoded ? Buffer.from(event.body, 'base64').toString() : event.body;
                console.log('Pub Key: '  + pubKey);

                const currentTimeEpoch = (new Date).getTime() / 1000;
                if (createdAt < currentTimeEpoch - 10) {
                    console.log('Expected createdAt: ' + createdAt + ' to be less than 10 seconds stale');
                    fail(callback, 'Expected createdAt: ' + createdAt + ' to be less than 10 seconds stale');
                    return;
                }

                const pubKeyHash = crypto.createHash('md5').update(pubKey).digest("hex");
                if (pubKeyMD5 !== pubKeyHash) {
                    console.log('Expected pubKeyMD5 is token: ' + pubKeyMD5 + ' to match md5 hash of pub key: ' + pubKeyHash);
                    fail(callback, 'Expected pubKeyMD5 is token: ' + pubKeyMD5 + ' to match md5 hash of pub key: ' + pubKeyHash);
                    return;
                }

                ddb.getItem({TableName: process.env.ClientSshPublicKeysTableName, Key: {clientId: {S: clientId}}}, function(err, data) {
                    if (err) {
                        console.log(err);
                        fault(callback, err);
                        return;
                    }

                    if (!data.Item) {
                        console.log('No existing public key in DDB for that client ID');
                        uploadNewPublicKey(callback, clientId, pubKey, function() {
                            console.log('Successfully created it');
                            succeed(callback, 'No existing public key in DDB for that client ID. Successfully uploaded it.');
                        })
                    } else if (data.Item.sshPubKey.S !== pubKey) {
                        console.log('Existing public key is different from the uploaded one');
                        uploadNewPublicKey(callback, clientId, pubKey, function() {
                            console.log('Successfully updated it');
                            succeed(callback, 'Existing different public key in DDB for that client ID. Successfully updated it.');
                        })
                    } else {
                        console.log('Existing public key matches the uploaded one');
                        succeed(callback, 'Existing public key matches the uploaded one')
                    }
                });
            })
        });


    } else {
        fail(callback, 'Unknown API: ' + path);
    }
}
